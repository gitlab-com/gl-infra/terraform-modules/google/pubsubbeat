variable "kernel_version" {
  default = ""
}

variable "use_new_node_name" {
  default = false
}

variable "names" {
  description = "Names of the indexes, one per pubsubbeat"
  type        = list(string)
}

variable "machine_types" {
  description = "Machine types for pubsubbeats"
  type        = list(string)
}

variable "allow_stopping_for_update" {
  description = "wether Terraform is allowed to stop the instance to update its properties"
  default     = "false"
}

variable "backend_protocol" {
  default = "HTTP"
}

variable "health_check" {
  default = "http"
}

variable "service_port" {
}

variable "service_path" {
  default = "/"
}

variable "block_project_ssh_keys" {
  type        = string
  description = "Whether to block project level SSH keys"
  default     = "TRUE"
}

variable "chef_init_run_list" {
  type        = string
  default     = ""
  description = "run_list for the node in chef that are ran on the first boot only"
}

variable "chef_provision" {
  type        = map(string)
  description = "Configuration details for chef server"
}

variable "chef_run_list" {
  type        = string
  description = "run_list for the node in chef"
}

variable "create_topics" {
  type        = bool
  default     = true
  description = "Control topics creation"
}

variable "dns_zone_name" {
  type        = string
  description = "The GCP name of the DNS zone to use for this environment"
}

variable "egress_ports" {
  type        = list(string)
  description = "The list of ports that should be opened for egress traffic"
  default     = []
}

variable "enable_oslogin" {
  type        = string
  description = "Whether to enable OS Login GCP feature"

  # Note: setting this to TRUE breaks chef!
  # https://gitlab.com/gitlab-com/gitlab-com-infrastructure/merge_requests/297#note_66690562
  default = "FALSE"
}

variable "environment" {
  type        = string
  description = "The environment name"
}

variable "log_disk_size" {
  type        = string
  description = "The size of the log disk"
  default     = 50
}

variable "log_disk_type" {
  type        = string
  description = "The type of the log disk"
  default     = "pd-standard"
}

variable "ip_cidr_range" {
  type        = string
  description = "The IP range"
}

variable "name" {
  default = "pubsub"
}

variable "os_boot_image" {
  type        = string
  description = "The OS image to boot"
  default     = "ubuntu-os-cloud/ubuntu-1604-lts"
}

variable "os_disk_size" {
  type        = string
  description = "The OS disk size in GiB"
  default     = 20
}

variable "os_disk_type" {
  type        = string
  description = "The OS disk type"
  default     = "pd-standard"
}

variable "preemptible" {
  type        = string
  description = "Use preemptible instances for this pet"
  default     = "false"
}

variable "project" {
  type        = string
  description = "The project name"
}

variable "public_ports" {
  type        = list(string)
  description = "The list of ports that should be publicly reachable"
  default     = []
}

variable "region" {
  type        = string
  description = "The target region"
}

variable "service_account_email" {
  type        = string
  description = "Service account emails under which the instance is running"
}

variable "teardown_version" {
  description = "version of the teardown script"
  default     = 1
}

variable "tier" {
  type        = string
  description = "The tier for this service"
}

variable "topic_names" {
  description = "Names of the topics, one per pubsubbeat"
  type        = list(string)
}

variable "vpc" {
  type        = string
  description = "The target network"
}

variable "zone" {
  type    = string
  default = ""
}

# Instances without public IPs cannot access the public internet without NAT.
# Ensure that a Cloud NAT instance covers the subnetwork/region for this
# instance.
variable "assign_public_ip" {
  type    = bool
  default = true
}

# Stackdriver's advanced search syntax is aplicable here
variable "gke_log_filter" {
  type    = string
  default = ""
}

# allows toggling creation of instances
# if false, we will only create topics
variable "create_instances" {
  type    = bool
  default = true
}
