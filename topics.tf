##################################
#
#  Pubsub topics for logging
#  one per pubsub host
#
##################################

# There's no need to create a subscription to the topic because pubsubbeat binary will create it if it doesn't already exist: https://github.com/GoogleCloudPlatform/pubsubbeat/blob/master/pubsubbeat.reference.yml#L28
# We configure pubsubbeat with the name of the topic which should already exist in GCP: https://ops.gitlab.net/gitlab-cookbooks/gitlab-elk/blob/master/attributes/default.rb#L52
# and a subscription name to be used when creating the subscription: https://ops.gitlab.net/gitlab-cookbooks/gitlab-elk/blob/master/attributes/default.rb#L53
resource "google_pubsub_topic" "mytopic" {
  count = var.create_topics == true ? length(var.topic_names) : 0
  name  = var.topic_names[count.index]
}
