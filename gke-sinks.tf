###############################################################
## Default log sink for all GKE related logs that are not
## an application log that will be configured to send directly
## to elasticsearch

# Searches topic_name for a topic named gke, if it exists
# it will be used for the default topic for all gke logs
locals {
  gke_topic = [
    for topic in var.topic_names :
    topic
    if length(regexall(".*-gke-.*", topic)) > 0
  ]
}

resource "google_logging_project_sink" "gke-default" {
  count = length(local.gke_topic) == 1 ? 1 : 0
  name  = "${var.environment}-pubsub-sink-gke-default"

  # There is only a single pubsub topic for gke
  destination = "pubsub.googleapis.com/projects/${var.project}/topics/${local.gke_topic[0]}"
  filter      = var.gke_log_filter

  # Use a unique writer (creates a unique service account used for writing)
  unique_writer_identity = true
  depends_on             = [google_pubsub_topic.mytopic]
}

resource "google_project_iam_binding" "log-writer" {
  role = "roles/pubsub.publisher"

  members = concat(
    google_logging_project_sink.gke-default.*.writer_identity,
    ["serviceAccount:${var.service_account_email}"]
  )
}
