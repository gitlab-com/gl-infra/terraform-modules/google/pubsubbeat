resource "google_compute_firewall" "public" {
  count   = length(var.public_ports) > 0 ? 1 : 0
  name    = format("%v-%v", var.name, var.environment)
  network = var.vpc

  allow {
    protocol = "tcp"
    ports    = var.public_ports
  }

  source_ranges = ["0.0.0.0/0"]

  target_tags = [var.name]
}

resource "google_compute_firewall" "deny_all_egress" {
  count     = length(var.egress_ports) > 0 ? 1 : 0
  name      = format("deny-all-egress-%v-%v", var.name, var.environment)
  network   = var.vpc
  direction = "EGRESS"
  priority  = 65000

  deny {
    protocol = "tcp"
  }

  destination_ranges = ["0.0.0.0/0"]

  target_tags = [var.name]
}

resource "google_compute_firewall" "to_network" {
  count     = length(var.egress_ports) > 0 ? 1 : 0
  name      = format("allow-network-egress-%v-%v", var.name, var.environment)
  network   = var.vpc
  direction = "EGRESS"
  priority  = 2000

  allow {
    protocol = "tcp"
  }

  destination_ranges = ["10.0.0.0/8"]

  target_tags = [var.name]
}

resource "google_compute_firewall" "to_world" {
  count     = length(var.egress_ports) > 0 ? 1 : 0
  name      = format("allow-world-egress-%v-%v", var.name, var.environment)
  network   = var.vpc
  direction = "EGRESS"

  allow {
    protocol = "tcp"
    ports    = var.egress_ports
  }

  destination_ranges = ["0.0.0.0/0"]

  target_tags = [var.name]
}

