output "instances_self_link" {
  value = google_compute_instance.default.*.self_link
}

output "instance_groups_self_link" {
  value = google_compute_instance_group.default.*.self_link
}

output "topic_names" {
  value = google_pubsub_topic.mytopic.*.name
}
