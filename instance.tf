module "bootstrap" {
  source = "git::ssh://git@ops.gitlab.net/gitlab-com/gl-infra/terraform-modules/google/bootstrap.git?ref=v4.0.2"
}

resource "google_compute_backend_service" "default" {
  count = var.create_instances ? length(data.google_compute_zones.available.names) : 0
  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    data.google_compute_zones.available.names[count.index],
  )
  protocol  = var.backend_protocol
  port_name = var.name

  backend {
    group = google_compute_instance_group.default[count.index].self_link
  }

  health_checks = [var.health_check == "http" ? google_compute_health_check.http.self_link : google_compute_health_check.tcp.self_link]
}

resource "google_compute_health_check" "tcp" {
  name = format("%v-%v-tcp", var.environment, var.name)

  tcp_health_check {
    port = var.service_port
  }
}

resource "google_compute_health_check" "http" {
  name = format("%v-%v-http", var.environment, var.name)

  http_health_check {
    port         = var.service_port
    request_path = var.service_path
  }
}

# Add one instance group per zone
# and only select the appropriate instances
# for each one

resource "google_compute_instance_group" "default" {
  count = var.create_instances ? length(data.google_compute_zones.available.names) : 0
  name = format(
    "%v-%v-%v",
    var.environment,
    var.name,
    data.google_compute_zones.available.names[count.index],
  )
  description = "Instance group for monitoring VM."
  zone        = data.google_compute_zones.available.names[count.index]

  named_port {
    name = var.name
    port = var.service_port
  }

  # This filters the full set of instances to only ones for the appropriate zone.
  instances = matchkeys(
    google_compute_instance.default.*.self_link,
    google_compute_instance.default.*.zone,
    [data.google_compute_zones.available.names[count.index]],
  )
}

resource "google_compute_disk" "log_disk" {
  project = var.project
  count   = var.create_instances ? length(var.names) : 0
  name = format(
    "%v-%02d-%v-%v-log",
    var.name,
    count.index + 1,
    var.tier,
    var.environment,
  )
  zone = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)
  size = var.log_disk_size
  type = var.log_disk_type

  labels = {
    environment = var.environment
  }
}

resource "google_compute_instance" "default" {
  allow_stopping_for_update = var.allow_stopping_for_update

  count = var.create_instances ? length(var.names) : 0
  name = format(
    "%v-%v-%v-%v",
    var.name,
    var.names[count.index],
    var.tier,
    var.environment,
  )
  machine_type = var.machine_types[count.index]

  metadata = {
    "CHEF_URL"     = var.chef_provision["server_url"]
    "CHEF_VERSION" = var.chef_provision["version"]
    "CHEF_NODE_NAME" = var.use_new_node_name ? format(
      "%v-%v-%v-%v.c.%v.internal",
      var.name,
      var.names[count.index],
      var.tier,
      var.environment,
      var.project,
      ) : format(
      "%v-%v.%v.%v.%v",
      var.name,
      var.names[count.index],
      var.tier,
      var.environment,
      var.dns_zone_name,
    )
    "CHEF_TOPIC_NAME"        = var.topic_names[count.index]
    "GL_KERNEL_VERSION"      = var.kernel_version
    "CHEF_ENVIRONMENT"       = var.environment
    "CHEF_INIT_RUN_LIST"     = var.chef_init_run_list
    "CHEF_RUN_LIST"          = var.chef_run_list
    "CHEF_DNS_ZONE_NAME"     = var.dns_zone_name
    "CHEF_PROJECT"           = var.project
    "CHEF_BOOTSTRAP_BUCKET"  = var.chef_provision["bootstrap_bucket"]
    "CHEF_BOOTSTRAP_KEYRING" = var.chef_provision["bootstrap_keyring"]
    "CHEF_BOOTSTRAP_KEY"     = var.chef_provision["bootstrap_key"]
    "block-project-ssh-keys" = var.block_project_ssh_keys
    "enable-oslogin"         = var.enable_oslogin
    "shutdown-script"        = module.bootstrap.teardown
    "startup-script"         = module.bootstrap.bootstrap
  }

  project = var.project
  zone    = var.zone != "" ? var.zone : element(data.google_compute_zones.available.names, count.index + 1)

  service_account {
    // this should be the instance under which the instance should be running, rather than the one creating it...
    email = var.service_account_email

    // all the defaults plus cloudkms to access kms
    scopes = [
      "https://www.googleapis.com/auth/cloud.useraccounts.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring.write",
      "https://www.googleapis.com/auth/pubsub",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/servicecontrol",
      "https://www.googleapis.com/auth/trace.append",
      "https://www.googleapis.com/auth/cloudkms",
      "https://www.googleapis.com/auth/compute.readonly",
    ]
  }

  scheduling {
    preemptible = var.preemptible
  }

  boot_disk {
    auto_delete = true

    initialize_params {
      image = var.os_boot_image
      size  = var.os_disk_size
      type  = var.os_disk_type
    }
  }

  attached_disk {
    source      = google_compute_disk.log_disk[count.index].self_link
    device_name = "log"
  }

  network_interface {
    subnetwork = google_compute_subnetwork.subnetwork[0].name
    dynamic "access_config" {
      for_each = var.assign_public_ip ? [0] : []
      content {
      }
    }
  }

  labels = {
    environment = var.environment
    pet_name    = "${var.name}-${var.names[count.index]}"
  }

  tags = [
    var.name,
    "${var.name}-${var.names[count.index]}",
    var.environment,
  ]
}
